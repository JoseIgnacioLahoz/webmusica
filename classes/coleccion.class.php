<?php 
	//Propieadades de la Classe
class Coleccion{
	public $discos;
	public $titulo;

	//Constructor
	public function __construct($titulo=''){
		$this->titulo=$titulo;
		$this->discos=[];

	}

	//Metodo agregar disco
	public function agregar($disco){
		$this->discos[]=$disco;
	}

	//Metodo listar discos
	public function listar(){
		return $this->discos;
	}

}

?>