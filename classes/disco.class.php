<?php 

class Disco{
	public $nombre;
	public $anio;
	public $autor;
	public $numcanciones;
	public $portada;

	//Constructor
	public function __construct($nom, $anio, $aut, $numcanciones=0, $portada){
		$this->nombre=$nom;
		$this->anio=$anio;
		$this->autor=$aut;
		$this->numcanciones=$numcanciones;
		$this->portada=$portada;
	}

	//Metodos
	public function dimeInfo(){
		return $this->nombre .' - '.$this->anio .' - '. $this->autor.' - '.$this->numcanciones;
	}

}

?>