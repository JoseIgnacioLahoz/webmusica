<?php 
    require('classes/coleccion.class.php');
	require('classes/disco.class.php');

//Primero compruebo si el usuario a rellenado un nuevo disco y lo inlcuyo en el fichero datos.txt

//Partida de  disco NO insertado
$discoInsertado=false;

if(isset($_POST['enviar'])){
	//Marco que voy a insertar un disco
	$discoInsertado=true;

	//todos los datos son correctos
	$datosCorrectos=true;
	$datosMensaje='';
	$tipoMensaje='danger';


	//Recojo los datos del disco comprobando cada campo
	$nombre=$_POST['nombre'];
	if(strlen($nombre)<2){
		$datosCorrectos=false;
		$datosMensaje.='El nombre del disco no es correcto<br>';
	}

	$anio=$_POST['anio'];
	if( (strlen($anio)!=4) OR (!is_numeric($anio)) ){
		$datosCorrectos=false;
		$datosMensaje.="El año debe ser un numero de 4 digitos<br>";
	}

	$autor=$_POST['autor'];
	if(strlen($autor)<2){
		$datosCorrectos=false;
		$datosMensaje.="El nombre delautor no es correcto<br>";
	}

	$numcanciones=$_POST['numcanciones'];
	if(!is_numeric($numcanciones)){
		$datosCorrectos=false;
		$datosMensaje.="El número de canciones no es correcto<br>";
	}

	//Guardar el fichero de imagen en su sitio comprobando y exigiendo una imagen de portada
	if(!is_uploaded_file($_FILES['portada']['tmp_name'])){
		$datosCorrectos=false;
		$datosMensaje.="Debes elegir una protada para el disco<br>";
	}
	if($datosCorrectos==true){

		$datosMensaje='Disco insertado con exito<br>';
		$tipoMensaje='success';

		move_uploaded_file($_FILES['portada']['tmp_name'], 'imagenes/'.$_FILES['portada']['name']);

		//Creo la línea que insertaré en el archivo de texto
		$linea="\r\n".$nombre.';'.$anio.';'.$autor.';'.$numcanciones.';'.$_FILES['portada']['name'];

		//Abro el fichero en modo agregar
		$fichero=fopen('datos.txt', 'a'); //para agregar al final

		//Escribo mi linea en el fichero
		fwrite($fichero, $linea);

		//Cierro el fichero
		fclose($fichero);
	}else{
		echo "Debes elegir una imagen de portada";
	}

}


	//creo objeto de la clase Coleccion
$coleccion=new Coleccion('Coleccion actualizada');

//Rellenar los contactos a partir de un fichero de texto
//funciones para abrir fichero en modo lectura
$fichero=fopen('datos.txt','r');

//Leer el fichero linea a linea obviando la primera linea
$linea=fgets($fichero);
while($linea=fgets($fichero)){
	$partes=explode(';',$linea);
	$nombre=trim($partes[0]);
	$anio=trim($partes[1]);
	$autor=trim($partes[2]);
	$numcanciones=trim($partes[3]);
	$portada=trim($partes[4]);
	$coleccion->agregar(new Disco($nombre, $anio, $autor, $numcanciones, $portada));
}

//Cerrar el fichero
fclose($fichero);

?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Web Música</title>
 
    <!-- CSS de Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" media="screen">

 
  </head>
  <body>
	<?php 
      include('includes/encabezado.php');
    ?>

    <!-- Si inserto nuvo disco muestro ventana -->
    <?php if($discoInsertado==true){ ?>
			<div class="alert alert-<?php echo $tipoMensaje;?>">
				<?php echo $datosMensaje;?>
			</div>
    	  <?php } ?>
    
	<!-- CONTENIDO CENTRAL DE LA PAGINA -->
  	<section class="container">
  	<section>
  		<h2 class="alert alert-success">
  			<a href="index.php">Gestión de Discos</a>
  			<small><?php echo $coleccion->titulo; ?></small>
  		</h2>
  	</section>
	
    <div class="row">
    	<div class="col-md-8">
		    <table class="table table-hover table-striped" >
				<!-- <tr>
					<th colspan="4"><?php echo $agenda->titulo; ?></th>
				</tr> -->

				<tr>
					<th>Nombre</th>
					<th>Año</th>
					<th>Autor</th>
					<th>Pistas</th>
					<th>Portada</th>
				</tr>

				<?php foreach ($coleccion->listar() as $disco) { ?>

					<tr>
						<td><?php echo $disco->nombre; ?></td>
						<td><?php echo $disco->anio; ?></td>
						<td><?php echo $disco->autor; ?></td>
						<td><?php echo $disco->numcanciones; ?></td>
						<td>
							<img src="imagenes/<?php echo $disco->portada; ?>" width="100">	
						</td>
					</tr>
				<?php } ?>

			</table>
		</div>

		<div class="col-md-4">
	
			<form action="index.php" method="post" enctype="multipart/form-data">
				<input type="text" class="form-control" name="nombre" placeholder="Nuevo Disco"><br>
				<input type="number" min="1800" max="<?php echo date('Y'); ?>" value="<?php echo date('Y'); ?>" class="form-control" name="anio" placeholder="Año del nuevo disco"><br>
				<input type="text" class="form-control" name="autor" placeholder="Autor del nuevo Disco"><br>
				<input type="number" min="1" max="100" value="1" class="form-control" name="numcanciones" placeholder="Nº de Canciones del nuevo Disco"><br>
				<input type="file" class="form-control" name="portada"><br>
				<input type="submit" name="enviar" value="Enviar"><br>
			</form>
		</div>
	</div>

	</section>
	<hr>
	<footer>
		<?php 
      		include('includes/pie.php');
    	?>
	</footer>



 
    <!-- Cargar Librería jQuery requerida por los plugins de JavaScript -->
    <script src="js/jquery-3.3.1.min.js"></script>
 
    <!-- Todos los plugins JavaScript de Bootstrap (también puedes
         incluir archivos JavaScript individuales de los únicos
         plugins que utilices) -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>